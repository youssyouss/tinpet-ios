//
//  ContentView.swift
//  tinpet
//
//  Created by Youssra on 18/04/2020.
//  Copyright © 2020 Youssra. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
